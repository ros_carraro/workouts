# cal/views.py

from datetime import datetime, timedelta, date
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.views import generic
from django.urls import reverse
from django.shortcuts import redirect
from django.utils.safestring import mark_safe
import calendar

from .models import *
from .utils import Calendar
from .forms import EventForm, VideoForm

def index(request):
    return HttpResponse('hello')

class CalendarView(generic.ListView):
    model = Workout
    template_name = 'cal/calendar.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # use today's date for the calendar
        d = get_date(self.request.GET.get('month', None))

        # Instantiate our calendar class with today's year and date
        cal = Calendar(d.year, d.month)

        # Call the formatmonth method, which returns our calendar as a table
        html_cal = cal.formatmonth(withyear=True)
        context['calendar'] = mark_safe(html_cal)

        # we calculate the previous and next month’s date in cal/views.py and pass it as template variables to our cal/calendar.html
        context['prev_month'] = prev_month(d)
        context['next_month'] = next_month(d)
        return context

def prev_month(d):
    first = d.replace(day=1)
    prev_month = first - timedelta(days=1)
    month = 'month=' + str(prev_month.year) + '-' + str(prev_month.month)
    return month

def next_month(d):
    days_in_month = calendar.monthrange(d.year, d.month)[1]
    last = d.replace(day=days_in_month)
    next_month = last + timedelta(days=1)
    month = 'month=' + str(next_month.year) + '-' + str(next_month.month)
    return month

def get_date(req_month):
    if req_month:
        year, month = (int(x) for x in req_month.split('-'))
        return date(year, month, day=1)
    return datetime.today()

def event(request, workout_id=None):
    instance = Workout()
    if workout_id:
        instance = get_object_or_404(Workout, pk=workout_id)
    else:
        instance = Workout()
    
    form = EventForm(request.POST or None, instance=instance)
    if request.POST and form.is_valid():
        form.save()
        return HttpResponseRedirect(reverse('cal:calendar'))
    return render(request, 'cal/workout.html', {'form': form})

def video(request, video_id=None):
    instance = Video()
    if video_id:
        instance = get_object_or_404(Video, pk=video_id)
    else:
        instance = Video()
    
    form = VideoForm(request.POST or None, instance=instance)
    if request.POST and form.is_valid():
        form.save()
        if video_id==None:
            # goes to the video_list page, because there is no ID
            return HttpResponseRedirect(reverse('cal:video_list'))
        else:
            # goes to the video_detail page of the video just edited
            return redirect('cal:video_detail', pk=video_id)
    return render(request, 'cal/video.html', {'form': form})

class VideoPListView(generic.ListView):
    model = Video
    paginate_by = 50


class VideoPDetailView(generic.DetailView):
    model = Video

class TagListView(generic.ListView):
    """The listing for tagged books."""
    template_name = "cal/video_list.html"

    def get_queryset(self):
        return Video.objects.filter(tags__slug=self.kwargs.get("slug")).all()

    def get_context_data(self, **kwargs):
        context = super(TagListView, self).get_context_data(**kwargs)
        context["tag"] = self.kwargs.get("slug")
        return context
