# cal/admin.py

from django.contrib import admin
from cal.models import Workout, Video
from embed_video.admin import AdminVideoMixin

class VideoPAdmin(AdminVideoMixin, admin.ModelAdmin):
    pass

admin.site.register(Workout)
admin.site.register(Video, VideoPAdmin)
