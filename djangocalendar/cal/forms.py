# cal/forms.py

from django.forms import ModelForm, DateInput
from cal.models import Workout, Video
from datetime import datetime   

class EventForm(ModelForm):
  class Meta:
    model = Workout
    # datetime-local is a HTML5 input type, format to make date time show on fields
    widgets = {
      'date': DateInput(attrs={'type': 'date', 'value': datetime.now().strftime("%Y-%m-%d")}, format='%Y-%m-%d'),
    }
    fields = '__all__'

  def __init__(self, *args, **kwargs):
    super(EventForm, self).__init__(*args, **kwargs)
    # input_formats to parse HTML5 datetime-local input to datetime field
    self.fields['date'].input_formats = ('%Y-%m-%d',)

class VideoForm(ModelForm):
  class Meta:
    model = Video
    fields = '__all__'

  def __init__(self, *args, **kwargs):
    super(VideoForm, self).__init__(*args, **kwargs)