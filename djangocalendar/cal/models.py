# cal/models.py

from django.db import models
from django.urls import reverse

# for YouTube
from embed_video.fields import EmbedVideoField
#for tags
from taggit.managers import TaggableManager
TAGGIT_CASE_INSENSITIVE = True

class Video(models.Model):
    title = models.CharField(max_length=200)
    #link = models.URLField(blank=True)
    video = EmbedVideoField(verbose_name="Video", help_text="Please enter video URL", blank=True)
    channel = models.CharField(max_length=50)
    tags = TaggableManager(blank=True)
    
    class Meta:
        ordering = ['id']

    def __str__(self):
        return ' - '.join([self.channel, self.title])

    def get_colored_name(self):
         return color_words(self.__str__())

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("cal:video_detail", kwargs={"pk": self.pk})

class Workout(models.Model):
    video = models.ForeignKey(Video, on_delete=models.CASCADE, blank=True, null=True)
    description = models.TextField()
    date = models.DateField()
    duration = models.IntegerField(default=-99)
    bpm = models.IntegerField(default=-99)
    calories = models.IntegerField(default=-99)
    
    class Meta:
        ordering = ['date']

    @property
    def get_html_url(self):
        url = reverse('cal:workout_edit', args=(self.id,))
        if self.video:
            return f'<a href="{url}"> {self.video.get_colored_name()} </a>'
        else:
            n = self.description.find('\n')
            if n == -1:
               string = color_words(self.description)
            else:
               string = color_words(self.description[:n])
            return f'<a href="{url}"> {string} </a>'

    def __str__(self):
        return self.description

def color_words(string):
   red = '#d11569'
   green = '#3a9278'
   yellow = '#f9c74f'
   blue = '#4f5bd5'
   light_green = '#00b300'
   purple = '#962fbf'
   light_blue = '#0099ff'
   dictionary={'abs':red, 'ab':red, 'abdomen':red, 'core':red, 'gutts':red, 'plank':red,
               'total':green, 'hiit':green, 'full':green,
               'knee':yellow, 'posture':yellow, 'rehab':yellow, 'fix':yellow, 'pain':yellow,
               'booty':blue, 'legs':blue, 'leg':blue, 'thigh':blue, 'butt':blue, 'butts':blue,
               'arms':light_green, 'upper':light_green, 'shoulders':light_green, 'chest':light_green, 'back':light_green, 'arm':light_green,
               'cardio':purple,
               'stretch':light_blue, 'stretching':light_blue, 'stretches':light_blue, 'flexibility':light_blue}
   words = string.split(" ")
   for i in range(0, len(words)):
      word = words[i]
      if word.lower() in dictionary.keys():
         words[i] = f'<span style="color: {dictionary[word.lower()]}; font-weight:bold"> {word} </span>'
   string=" ".join(words)
   return string
