# cal/urls.py

from django.conf.urls import url
from . import views

app_name = 'cal'
urlpatterns = [
    url(r'^index/$', views.index, name='index'),
    url(r'^calendar/$', views.CalendarView.as_view(), name='calendar'),
    url(r'^workout/new/$', views.event, name='workout_new'),
    url(r'^workout/edit/(?P<workout_id>\d+)/$', views.event, name='workout_edit'),
    url(r'^video/new/$', views.video, name='video_new'),
    url(r'^video/edit/(?P<video_id>\d+)/$', views.video, name='video_edit'),
    url(r"video/(?P<pk>\d+)/$", views.VideoPDetailView.as_view(), name="video_detail"),
    url(r"video/$", views.VideoPListView.as_view(), name="video_list"),
    url(r'^video/tagged/(?P<slug>[-\w]+)/$', views.TagListView.as_view(), name="tagged_videos"),
]
