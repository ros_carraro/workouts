# README #

### What is this repository for? ###

This is a calendar for logging workouts performed with YouTube videos. It has a monthly calendar view and a videos view. Videos can be categorized by adding tags. It also allows to enter burnt calories and other parameters from your smartwatch. Workout names in the calendar view will be colored based on keywords.

The calendar view is based on [huiwenhw's calendar](https://github.com/huiwenhw/django-calendar).

### How do I get set up? ###

* Summary of set up

The code is based on Django. To install it with all dependencies create a conda environment by running:

```bash
conda create -n workouts -c conda-forge django
conda activate workouts
pip install django-embed-video
pip install django-taggit
```
* Configuration

To get it running 
```bash
git clone https://ros_carraro@bitbucket.org/ros_carraro/workouts.git
cd workouts/djangocalendar

python3 manage.py migrate
python3 manage.py runserver
```
Your workout calendar will be at [http://localhost:8000/calendar/](http://localhost:8000/calendar/).

You can go to the Videos page to add new videos via their link, watch them and then log the workouts you just did!

### License ###
This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/).

[![license](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)

![Calendar_page](Screenshots/Calendar_page.jpg)

---------------------------------

![Videos_page](Screenshots/Videos_page.jpg)

------------------------------------

![Video_detail](Screenshots/Video_detail.jpg)